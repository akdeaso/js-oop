class Grid {
  constructor(options) {
    this.columns = options.columns;
    this.data = options.data;
  }

  createTableHeader() {
    let header = "<thead><tr>";
    for (const column of this.columns) {
      header += `<th>${column}</th>`;
    }
    header += "</tr></thead>";
    return header;
  }

  createTableBody() {
    let body = "<tbody>";
    for (const rowData of this.data) {
      body += "<tr>";
      for (const cellData of rowData) {
        body += `<td>${cellData}</td>`;
      }
      body += "</tr>";
    }
    body += "</tbody>";
    return body;
  }

  render(containerId) {
    const gridContainer = document.getElementById(containerId);
    const table = document.createElement("table");
    table.classList.add("table", "table-hover");
    table.innerHTML = this.createTableHeader() + this.createTableBody();
    gridContainer.appendChild(table);
  }
}

const gridData = {
  columns: ["Name", "Email", "Phone Number"],
  data: [
    ["John", "john@example.com", "(353) 01 222 3333"],
    ["Mark", "mark@gmail.com", "(01) 22 888 4444"],
  ],
};

const grid = new Grid(gridData);
grid.render("grid");
